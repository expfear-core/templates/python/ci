image: python:latest

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  PIP_INDEX_URL: $DEVPI_SIMPLE_INDEX
  PIP_TRUSTED_HOST: $DEVPI_HOST

cache:
  key: $CI_COMMIT_REF_SLUG
  paths:
    - .cache/pip
    - venv/


before_script:
  - python -V
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate


stages:
  - setup
  - test
  - doc
  - review
  - close_review
  - build
  - deploy


env_setup:
  stage: setup
  script:
    - pip install -e .
    - pip install -r requirements_test.txt
    - pip install -r requirements_doc.txt


unit_testing:
  stage: test
  script:
    - coverage run -m pytest
    - coverage report
    - coverage xml
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    expire_in: 4 week
  except:
    - tags


linter:
  stage: test
  script:
    - pylint ./src
  except:
    - tags


security test:
  stage: test
  script:
    - bandit -r ./src
  except:
    - tags


api_review:
  stage: review
  image: docker:19.03.12
  services:
    - docker:19.03.12-dind
  script:
    - IMAGE_NAME="$CI_REGISTRY/$CI_PROJECT_PATH/$CI_PROJECT_NAME"
    - echo $IMAGE_NAME:$CI_COMMIT_REF_SLUG >> version.txt
    - DYNAMIC_ENVIRONMENT_URL="$IMAGE_NAME"
    - echo "DYNAMIC_ENVIRONMENT_URL=$DYNAMIC_ENVIRONMENT_URL" >> deploy.env
    - docker build -t $IMAGE_NAME:$CI_COMMIT_REF_SLUG .
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - docker push $IMAGE_NAME:$CI_COMMIT_REF_SLUG
  artifacts:
    reports:
      dotenv: deploy.env
    paths:
      - version.txt
    expire_in: 4 week
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: $DYNAMIC_ENVIRONMENT_URL
    on_stop: stop_review
    auto_stop_in: 4 week
  only:
    - branches
  except:
    - master


stop_review:
  stage: close_review
  dependencies:
    - api_review
  image: docker:19.03.12
  services:
    - docker:19.03.12-dind
  variables:
    GIT_STRATEGY: none
    REG_SHA256: ade837fc5224acd8c34732bf54a94f579b47851cc6a7fd5899a98386b782e228
    REG_VERSION: 0.16.1
  before_script:
    - apk add --no-cache curl
    - curl --fail --show-error --location "https://github.com/genuinetools/reg/releases/download/v$REG_VERSION/reg-linux-amd64" --output /usr/local/bin/reg
    - echo "$REG_SHA256  /usr/local/bin/reg" | sha256sum -c -
    - chmod a+x /usr/local/bin/reg
  script:
    - IMAGE_TAG="$(cat version.txt)"
    - /usr/local/bin/reg rm -d --auth-url $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $IMAGE_TAG
  when: manual
  environment:
      name: review/$CI_COMMIT_REF_SLUG
      action: stop
  only:
    - branches
  except:
    - master


pages:
  stage: doc
  script:
    - gitchangelog > docs/source/changelog.rst
    - sphinx-build -b html docs/source public
  artifacts:
    paths:
      - public
    expire_in: 1 hour
  only:
    - tags


build:
  image: docker:19.03.12
  stage: build
  services:
    - docker:19.03.12-dind
  script:
    - IMAGE_NAME="$CI_REGISTRY/$CI_PROJECT_PATH/$CI_PROJECT_NAME"
    - docker build -t $IMAGE_NAME:latest -t $IMAGE_NAME:$CI_COMMIT_TAG .
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - docker push $IMAGE_NAME:$CI_COMMIT_TAG
    - docker push $IMAGE_NAME:latest
  only:
    - tags
